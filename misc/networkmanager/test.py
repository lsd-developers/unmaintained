#!/usr/bin/python2

try:
    import dbus
    bus = dbus.SystemBus()
    obj = bus.get_object('org.freedesktop.NetworkManager', '/org/freedesktop/NetworkManager')
    t = dbus.Interface(obj, "org.freedesktop.NetworkManager")
    t.GetDevices()
except Exception as detail:
    print(detail)