if [[ -x /usr/bin/plymouth && -x /usr/sbin/plymouthd ]]; then
	ply_client() { /usr/bin/plymouth --ping && /usr/bin/plymouth "$@"; }
	ply_daemon() { /usr/sbin/plymouthd --pid-file=/run/plymouth.pid "$@"; }

	# save a function under a new name
	save_function() {
		local ORIG=$(declare -f $1)
		eval "$2${ORIG#$1}"
	}

	save_function msg_busy std_stat_busy
	save_function msg_fail std_stat_fail

	# overwrite status functions
	msg_busy() {
		ply_client --update="$1"
		ply_client message --text="$1"
		std_stat_busy  "$@"
	}

	msg_fail() {
		ply_client --quit
		std_stat_fail "$@"
	}

	# update after local filesystems are mounted
	ply_sysinit() {
        ply_daemon --mode=boot
        ply_client --show-splash
    }
	add_hook sysinit_start ply_sysinit

    ply_settle() {
         ply_client update-root-fs --read-write
    }
    add_hook sysinit_leftovers ply_settle

	# stop plymouth after rc.multi
	ply_quit_multi() {
		ply_client quit
	}
    
	add_hook multi_end ply_quit_multi
    
	ply_quit_shutdown() {
		ply_client quit --retain-splash
	}
	add_hook shutdown_poweroff ply_quit_shutdown

	# start plymouth at the beginning of rc.shutdown
	ply_shutdown_start(){
		XPID=$(pidof X)
		if [ "$XPID" ]; then
			if [ "$(runlevel | cut -c 3)" != '5' ]; then
				local DM
				for DM in slim gdm kdm xdm entrance; do
					[ -f "/run/daemons/$DM" ] && rc.d stop "$DM"
				done
			fi
			kill -9 "$XPID" &> /dev/null
		fi
		ply_daemon --mode=shutdown
		ply_client --show-splash

		# don't get killed by kill_all
		pid_add_omit $(cat /run/plymouth.pid)
	}
	add_hook shutdown_start ply_shutdown_start

fi
