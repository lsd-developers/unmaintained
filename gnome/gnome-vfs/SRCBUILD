version=2.24.4
description="The GNOME Virtual File System"
depends=('gconf' 'bzip2' 'avahi' 'samba' 'gnome-mime-data' 'krb5' 'gnutls' 'libgcrypt')
makedepends=('pkg-config' 'intltool' 'gtk-doc' 'gnome-common')
sources=("http://ftp.gnome.org/pub/gnome/sources/gnome-vfs/2.24/gnome-vfs-$version.tar.bz2"
        gnutls-config.patch
        gcrypt-config.patch
        enable-deprecated.patch)

src_compile() {
  cd gnome-vfs-$version
  #Fix build with new gnutls
  patch -Np1 -i ../gnutls-config.patch
  #fix build with new libgcrypt >= 1.5.0
  patch -Np1 -i ../gcrypt-config.patch
  # remove -DG_DISABLE_DEPRECATED
  patch -Np1 -i ../enable-deprecated.patch

  sed -i -s 's|$(srcdir)/auto-test|auto-test|' test/Makefile.am

  libtoolize --force
  gtkdocize
  aclocal
  autoconf
  automake --add-missing
  CFLAGS="$CFLAGS -fno-strict-aliasing" ./configure \
      --prefix=/usr --sysconfdir=/etc \
      --localstatedir=/var --disable-static \
      --libexecdir=/usr/lib/gnome-vfs-2.0 \
      --enable-samba --with-samba-includes=/usr/include/samba-4.0 \
      --disable-hal --enable-avahi --disable-howl \
      --disable-openssl --enable-gnutls
  make
}

src_install() {
  cd gnome-vfs-$version
  make GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL=1 DESTDIR="$INSTALL_DIR" install

  install -d -m755 "$INSTALL_DIR/usr/share/gconf/schemas"
  gconf-merge-schema "$INSTALL_DIR/usr/share/gconf/schemas/gnome-vfs.schemas" --domain gnome-vfs-2.0 $INSTALL_DIR/etc/gconf/schemas/*.schemas
  rm -f $INSTALL_DIR/etc/gconf/schemas/*.schemas
}
