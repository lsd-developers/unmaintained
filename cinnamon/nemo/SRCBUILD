version=2.0.6
description="Cinnamon file manager (Nautilus fork)"
depends=('libexif' 'gvfs' 'dconf' 'desktop-file-utils' 'exempi' 'python2'
         'cinnamon-desktop' 'gnome-icon-theme' 'libnotify' 'libxml2'
         'cinnamon-translations' 'tracker')
makedepends=('gtk-doc' 'intltool' 'gnome-common')
sources=("https://github.com/linuxmint/nemo/tarball/$version"
        'no-docs.patch')

src_compile() {
    cd "$SOURCE_DIR/linuxmint-nemo-"*

    patch -Np0 -i "$SOURCE_DIR/no-docs.patch"

    # Fix build
    sed -i '/AC_SUBST(DISABLE_DEPRECATED_CFLAGS)/d' configure.in

    # Rename 'Files' app name to avoid having the same as nautilus
    sed -i 's/^Name\(.*\)=.*/Name\1=Nemo/' data/nemo.desktop.in.in

    ./autogen.sh
    ./configure --prefix=/usr \
        --sysconfdir=/etc \
        --localstatedir=/var --disable-static \
        --libexecdir=/usr/lib/nemo \
        --disable-update-mimedb \
        --disable-packagekit \
        --disable-gtk-doc-html \
        --disable-schemas-compile
    make
}

src_install() {
    cd "$SOURCE_DIR/linuxmint-nemo-"*
    make DESTDIR="$INSTALL_DIR" install
}
